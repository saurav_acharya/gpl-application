﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// helps to draw a line from one pont to another
    /// inherit from parent class shape
    /// </summary>
     public class drawTo:Shape
    {
        public int posLineX1, posLineX2;

        

        public drawTo() : base()
        {

        }
        public drawTo(Color colour, int positionX, int positionY, int posLineX1, int posLineX2) : base(colour, positionX, positionY)
        {
            this.posLineX1 = posLineX1;
            this.posLineX2 = posLineX2;
        }
        /// <summary>
        /// This method helps to set color and the position of x-axis and y-axis
        /// </summary>
        /// <param name="colour">To set color</param>
        /// <param name="list">array to set x and y position</param>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0],list[1]);
            this.posLineX1 = list[2];
            this.posLineX2 = list[3];
        }
        /// <summary>
        /// This method helps to draw a line from one point to another
        /// </summary>
        /// <param name="g">to set graphics</param>
        /// <param name="on_off">set to fill the circle</param>
        public override void draw(Graphics g, string on_off)
        {
            if (on_off.ToUpper() == "ON")
            {
                Pen p = new Pen(Color.Black, 2);
                g.DrawLine(p, positionX, positionY, posLineX1, posLineX2);
            }
            else
            {
                Pen p = new Pen(Color.Black, 2);
                g.DrawLine(p, positionX, positionY, posLineX1, posLineX2);
            }
            
        }
        
    }
}
