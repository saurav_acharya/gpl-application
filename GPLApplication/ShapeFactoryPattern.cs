﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// this class makes easier to choose the shape 
    /// helps to go to the class according to the shape
    /// </summary>
    class ShapeFactoryPattern
    {
        /// <summary>
        /// This method pass the type of shape
        /// </summary>
        /// <param name="shapeType">Passing the shape name in string such as:circle,rectangle,drawto to call their class</param>
        /// <returns></returns>
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim();
            //Checking the shapeType to be implemented

            if (shapeType.Equals("CIRCLE"))
            {
                return new Circle();

            }
            else if (shapeType.Equals("DRAWTO"))
            {
                return new drawTo();
            }
            else if (shapeType.Equals("RECTANGLE"))
            {
                return new Rectangle();
            }
            else if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle();
            }
            else
            {
                //if we get here then what has been passed in is inkown so throw an appropriate exception
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }
        }
    }
}
