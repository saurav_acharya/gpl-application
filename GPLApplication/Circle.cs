﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// created circle class which inherit from shape class
    /// It has the method and properties to draw a circle
    /// </summary>
    class Circle:Shape
    {
        int radius;

        public Circle() : base()
        {

        }
        /// <summary>
        /// Pariameterized constructer of circle calling the parent constructer
        /// </summary>
        /// <param name="colour">Holds colour to be drawn</param>
        /// <param name="positionX">holds the x-position</param>
        /// <param name="positionY">holds y-position</param>
        /// <param name="radius">holds the radius of circle</param>
        public Circle(Color colour, int positionX, int positionY, int radius) : base(colour, positionX, positionY)
        {
            this.radius = radius;
        }
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.radius = list[2];
        }
        /// <summary>
        /// This method helps to draw a circle in drawing area
        /// </summary>
        /// <param name="g">to set graphics</param>
        /// <param name="on_off">set to fill the circle</param>
        public override void draw(Graphics g, string on_off)
        {
            //checking condition of filling color in shape
            if (on_off.ToUpper() == "ON")
            {
                Pen p = new Pen(colour, 2);
                SolidBrush b = new SolidBrush(colour);
                g.FillEllipse(b, positionX, positionY, radius*2, radius*2);
                g.DrawEllipse(p, positionX, positionY, radius * 2, radius * 2);
            }
            else
            {
                Pen p = new Pen(colour, 2);
                g.DrawEllipse(p, positionX, positionY, radius * 2, radius * 2);
            }
           

        }
       
    }
}
