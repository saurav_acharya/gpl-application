﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// created circle class which inherit from shape class
    /// It has the method and properties to draw a circle
    /// </summary>
    class Rectangle :Shape
    {
        int width, height;
        public Rectangle() : base()
        {
            /*width = 100;
            height = 100;*/
        }
        /// <summary>
        /// Parameterized constructor of rectangle calling the parent constructer
        /// </summary>
        /// <param name="colour">Holds colour to be drawn</param>
        /// <param name="positionX">holds the x-position</param>
        /// <param name="poitionY">holds the y-position</param>
        /// <param name="width">holds the width</param>
        /// <param name="height">holds the height</param>
        public Rectangle(Color colour, int positionX, int poitionY, int width, int height) : base(colour, positionX, poitionY)
        {

            this.width = width; //the only thingthat is different from shape
            this.height = height;
        }

        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is width, list[3] is height
            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];

        }
        /// <summary>
        /// this method hepls to draw rectangle 
        /// </summary>
        /// <param name="g">draw shape</param>
        /// <param name="on_off">checks to fill the shape</param>
        public override void draw(Graphics g, string on_off)
        {
            if (on_off.ToUpper() == "ON")
            {
                Pen p = new Pen(colour, 2);
                SolidBrush b = new SolidBrush(colour);
                g.FillRectangle(b, positionX, positionY, width, height);
                g.DrawRectangle(p, positionX, positionY, width, height);
            }
            else
            {
                Pen p = new Pen(colour, 2);
                g.DrawRectangle(p, positionX, positionY, width, height);
            }
           
        }
       
    }
}
