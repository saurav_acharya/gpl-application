﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// creating the abstract class to create the base of other classes
    /// </summary>
    public abstract class Shape:ShapeInterface
    {
        protected Color colour;
        protected int positionX, positionY;

        public Shape()
        {
            colour = Color.Red;
            positionX = positionY = 100;
        }
        public Shape(Color colour, int positionX, int positionY)
        {
            this.colour = colour;
            this.positionX = positionX;
            this.positionY = positionY;
        }
        /// <summary>
        /// This method will be implemented by child class to draw the shape
        /// </summary>
        /// <param name="g">to draw shape</param>
        /// <param name="on_off">whether to fill or not the shape</param>
        public abstract void draw(Graphics g, string on_off);
       
        /// <summary>
        /// this method will help to set the color, x-axis and y-axis
        /// </summary>
        /// <param name="colour">to set color</param>
        /// <param name="list">integer array to add different value according to the class</param>
        public virtual void set(Color colour, params int[] list)
        {
            this.colour = colour;
            this.positionX = list[0];
            this.positionY = list[1];
        }
    }
}
