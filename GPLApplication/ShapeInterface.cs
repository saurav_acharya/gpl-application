﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Creates an inteface with the methods needed for the shape class
    /// </summary>
    interface ShapeInterface
    {
        /// <summary>
        /// To draw shape
        /// </summary>
        /// <param name="g"></param>
        /// <param name="on_off"></param>
        void draw(Graphics g, string on_off);
       /// <summary>
       /// To set color
       /// </summary>
       /// <param name="colour"></param>
       /// <param name="list"></param>
        void set(Color colour, params int[] list);
    }
}
