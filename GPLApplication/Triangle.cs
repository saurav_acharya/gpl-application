﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// created triangle class which inherit from shape class
    /// It has the method and properties to draw a triangle
    /// </summary>
    class Triangle :Shape
    {
        int firstSide, secondSide, thirdSide, fourthSide;

        public Triangle() : base()
        {

        }
        /// <summary>
        /// Pariameterized constructer of triangle calling the parent constructure
        /// </summary>
        /// <param name="colour">Holds colour to be drawn</param>
        /// <param name="positionX">holds the x-position</param>
        /// <param name="positionY">holds the y-position</param>
        /// <param name="firstSide">holds thirs point of x-axis</param>
        /// <param name="secondSide">holds fourth point of y-axis</param>
        /// <param name="thirdSide">holds the fifth point of x-axis</param>
        /// <param name="fourthSide">holds the sixth point of y-axis</param>
        public Triangle(Color colour, int positionX, int positionY, int firstSide, int secondSide, int thirdSide, int fourthSide) : base(colour, positionX, positionY)
        {
            this.firstSide = firstSide;
            this.secondSide = secondSide;
            this.thirdSide = thirdSide;
            this.fourthSide = fourthSide;
        }
        /// <summary>
        /// To set color position and different sides of triangle
        /// </summary>
        /// <param name="colour">To set color</param>
        /// <param name="list">array to pass position and sides of triangle</param>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.firstSide = list[2];
            this.secondSide = list[3];
            this.thirdSide = list[4];
            this.fourthSide = list[5];
        }
        /// <summary>
        /// this menthod hepls to draw triangle 
        /// point array is used to add positions to array
        /// </summary>
        /// <param name="g">To draw triangle</param>
        /// <param name="on_off">check to fill the triangle</param>
        public override void draw(Graphics g, string on_off)
        {
            Point[] trianglePoints = { new Point(positionX, positionY), new Point(firstSide, secondSide), new Point(thirdSide, fourthSide) };
           /* trianglePoints[0] = new Point(positionX, positionY);
            trianglePoints[1] = new Point(firstSide, secondSide);
            trianglePoints[2] = new Point(thirdSide, fourthSide);*/
            if (on_off.ToUpper() == "ON")
            {
                Pen p = new Pen(colour, 2);
                SolidBrush b = new SolidBrush(colour);
                g.FillPolygon(b, trianglePoints);
                /*g.DrawLine(p,trianglePoints[1],trianglePoints[2]);*/
                g.DrawPolygon(p,trianglePoints);
                
               
            }
            else
            {
                Pen p = new Pen(colour, 2);
                /*g.DrawLine(p,trianglePoints[1],trianglePoints[2]);*/
                g.DrawPolygon(p, trianglePoints);
                /*g.DrawLines(p, trianglePoints);*/
            }
            
        }
    }
}
