﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GPLApplication
{
    /// <summary>
    /// In this class all the validation and drawing shapes takes place
    /// Loop command are passed 
    /// variable are created to draw shapes
    /// </summary>
     public class CommandParser
    {
        int positionXone = 0;
        int positionYone = 0;
        string on_off = "off";
        Color finalColor = Color.Black;
        int num = 0;
        int x = 0;
        public string variable;
        public string comparator;
        public string value;
        public int VariableValue = 0;
        Boolean isAVariable = false;
        public IDictionary<string, int> VariableDictionary = new Dictionary<string, int>();
        public IDictionary<string, int> MethodVariableDictionary = new Dictionary<string, int>();
        public Dictionary<string, List<string>> MethodDictionary = new Dictionary<string, List<string>>();

        /// <summary>
        /// This methods checks whecther to run clear or reset the code
        /// </summary>
        /// <param name="singleLine">helps to choose whether to run clear or reset the graphics</param>
        /// <param name="multiLine">passes the multiple to draw shapes</param>
        /// <param name="g">to draw shapes on picture box</param>
        /// <param name="startone">initial value of the multiple line code</param>
        /// <param name="finishPoint">how many lines of code are there</param>
        public void CommandCode(string singleLine, string multiLine, Graphics g, int startone, int finishPoint)
        {
            if (singleLine == "RUN")
            {
                multiCommand(multiLine, g, startone, finishPoint);
            }
            else if (singleLine == "CLEAR")
            {
                g.Clear(SystemColors.Window);
            }
            else if (singleLine == "RESET")
            {
                positionXone = 0;
                positionYone = 0;
                g.Clear(SystemColors.Window);


                /* pictureBox1.Refresh();
                  richTextBox1.Clear();*/
                finalColor = Color.Black;
            }
            else
            {
                MessageBox.Show("Please pass valid command to run code: RUN,CLEAR,RESET");
            }
        }
        /// <summary>
        /// this method validates and check the code
        /// shows the error if there is
        /// </summary>
        /// <param name="multiCmd">all the code to be executed</param>
        /// <param name="g">to draw shapes on picture box</param>
        /// <param name="startone">initial value of the multiple line code</param>
        /// <param name="finishPoint">how many lines of code are there</param>
        public void multiCommand(string multiCmd, Graphics g, int startone, int finishPoint)
        {
            string[] multiLine = multiCmd.Split('\n');//spliting and storing multi line command in array 
            for (int index = startone; index < finishPoint; index++)//loop to parse string one by one
            {
                string[] readCmd = multiLine[index].Split(' ', ',');
                //To draw circle if circle command is passed
                if (readCmd[0].ToUpper() == "CIRCLE")
                {
                    if (readCmd.Length == 2)
                    {
                        int rad = 0;
                        try
                        {
                            rad = int.Parse(readCmd[1]);

                        }
                        catch (Exception ex)
                        {
                            rad = VariableDictionary[readCmd[1]];
                        }


                        string radius = (rad * 2).ToString();
                        ShapeFactoryPattern patterns = new ShapeFactoryPattern();
                        Shape shape = patterns.getShape(readCmd[0]);
                        /* shape.draw(returnedValue, g, positionX, positionY);*/
                        shape.set(finalColor, positionXone, positionYone, int.Parse(radius));
                        shape.draw(g, on_off);
                    }
                    else
                    {
                        MessageBox.Show("Please pass valid parameter for circle");
                    }
                }
                else if (readCmd[0].ToUpper() == "MOVETO")
                {
                    if (readCmd.Length == 3)
                    {


                        try
                        {
                            positionXone = int.Parse(readCmd[1]);
                            positionYone = int.Parse(readCmd[2]);
                        }
                        catch (FormatException exx)
                        {
                            try
                            {
                                positionXone = VariableDictionary[readCmd[1]];
                                positionYone = int.Parse(readCmd[2]);
                            }
                            catch (FormatException exc)
                            {
                                try
                                {
                                    positionXone = int.Parse(readCmd[2]);
                                    positionYone = VariableDictionary[readCmd[2]];
                                }
                                catch (FormatException el)
                                {
                                    try
                                    {
                                        positionXone = VariableDictionary[readCmd[1]];
                                        positionYone = VariableDictionary[readCmd[1]];
                                        /* MessageBox.Show("Please pass integer value after MOVETO");*/
                                    }
                                    catch (FormatException q)
                                    {
                                        MessageBox.Show("Please pass integer value after MOVETO");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please pass valid parameter for MOVETO");
                    }
                }
                else if (readCmd[0].ToUpper() == "DRAWTO")
                {
                    if (readCmd.Length == 3)
                    {
                        int oneLength = 0;
                        int twoLength = 0;
                        try
                        {
                            oneLength = int.Parse(readCmd[1]);
                            twoLength = int.Parse(readCmd[2]);
                        }
                        catch (Exception e)
                        {
                            try
                            {
                                oneLength = VariableDictionary[readCmd[1]];
                                twoLength = int.Parse(readCmd[2]);
                            }
                            catch (FormatException ex)
                            {
                                try
                                {
                                    oneLength = int.Parse(readCmd[1]);
                                    twoLength = VariableDictionary[readCmd[2]];
                                }
                                catch (FormatException el)
                                {
                                    oneLength = VariableDictionary[readCmd[1]];
                                    twoLength = VariableDictionary[readCmd[2]];
                                }
                            }
                            /* MessageBox.Show("Please pass integer value after DRAWTO");*/
                        }
                        ShapeFactoryPattern patterns = new ShapeFactoryPattern();
                        Shape shape = patterns.getShape(readCmd[0]);
                        shape.set(finalColor, positionXone, positionYone, oneLength, twoLength);
                        shape.draw(g, on_off);
                        /* int posLineX1 = int.Parse(returnedValue[1]);
                         int posLineY1 = int.Parse(returnedValue[2]);
                         Pen myPen = new Pen(Color.Black, 2);
                         g.DrawLine(myPen, positionXone, positionYone, posLineX1, posLineY1);*/
                    }
                    else
                    {
                        MessageBox.Show("Please pass valid parameter for DRAWTO");
                    }
                }
                else if (readCmd[0].ToUpper() == "RECTANGLE")
                {
                    if (readCmd.Length == 3)
                    {
                        int oneWidth = 0;
                        int oneHeight = 0;
                        try
                        {
                            oneWidth = int.Parse(readCmd[1]);
                            oneHeight = int.Parse(readCmd[2]);
                        }
                        catch (Exception e)
                        {
                            try
                            {
                                oneWidth = VariableDictionary[readCmd[1]];
                                oneHeight = int.Parse(readCmd[2]);
                            }
                            catch (FormatException ex)
                            {
                                try
                                {
                                    oneWidth = int.Parse(readCmd[1]);
                                    oneHeight = VariableDictionary[readCmd[2]];
                                }
                                catch (FormatException el)
                                {
                                    oneWidth = VariableDictionary[readCmd[1]];
                                    oneHeight = VariableDictionary[readCmd[2]];
                                }
                            }
                            /* MessageBox.Show("Please pass integer value after DRAWTO");*/
                        }
                        ShapeFactoryPattern patterns = new ShapeFactoryPattern();
                        Shape shape = patterns.getShape(readCmd[0]);
                        shape.set(finalColor, positionXone, positionYone, oneWidth, oneHeight);
                        shape.draw(g, on_off);
                    }
                    else
                    {
                        MessageBox.Show("Please pass valid parameter for RECTANGLE");
                    }
                }
                else if (readCmd[0].ToUpper() == "PEN")
                {
                    if (readCmd.Length == 2)
                    {
                        try
                        {
                            string colour = readCmd[1];
                            finalColor = Color.FromName(colour);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Please pass color name after PEN");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please pass valid parameter for PEN");
                    }
                }
                else if (readCmd[0].ToUpper() == "FILL")
                {
                    if (readCmd.Length == 2)
                    {
                        try
                        {
                            on_off = readCmd[1];
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Please pass ON/OFF value after Fill");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please pass valid parameter for FILL");
                    }
                }
                else if (readCmd[0].ToUpper() == "TRIANGLE")
                {
                    if (readCmd.Length == 5)
                    {
                        int oneside = 0;
                        int twoside = 0;
                        int thirdside = 0;
                        int fourside = 0;
                        try
                        {
                            oneside = int.Parse(readCmd[1]);
                            twoside = int.Parse(readCmd[2]);
                            thirdside = int.Parse(readCmd[3]);
                            fourside = int.Parse(readCmd[4]);

                        }
                        catch (Exception e)
                        {
                            try
                            {
                                oneside = VariableDictionary[readCmd[1]];
                                twoside = int.Parse(readCmd[2]);
                                thirdside = int.Parse(readCmd[3]);
                                fourside = int.Parse(readCmd[4]);
                            }
                            catch (FormatException wl)
                            {
                                try
                                {
                                    oneside = int.Parse(readCmd[1]);
                                    twoside = VariableDictionary[readCmd[2]];
                                    thirdside = int.Parse(readCmd[3]);
                                    fourside = int.Parse(readCmd[4]);
                                }
                                catch (FormatException rl)
                                {
                                    try
                                    {
                                        oneside = int.Parse(readCmd[1]);
                                        twoside = int.Parse(readCmd[2]);
                                        thirdside = VariableDictionary[readCmd[3]];
                                        fourside = int.Parse(readCmd[4]);
                                    }
                                    catch (FormatException ql)
                                    {
                                        try
                                        {
                                            oneside = int.Parse(readCmd[1]);
                                            twoside = int.Parse(readCmd[2]);
                                            thirdside = int.Parse(readCmd[3]);
                                            fourside = VariableDictionary[readCmd[4]];
                                        }
                                        catch (FormatException ol)
                                        {
                                            oneside = VariableDictionary[readCmd[1]];
                                            twoside = VariableDictionary[readCmd[2]];
                                            thirdside = VariableDictionary[readCmd[3]];
                                            fourside = VariableDictionary[readCmd[4]];
                                        }
                                    }
                                }
                            }
                            /*MessageBox.Show("Please pass integer value after TRIANGLE");*/
                        }
                        ShapeFactoryPattern patterns = new ShapeFactoryPattern();
                        Shape shape = patterns.getShape(readCmd[0]);

                        shape.set(finalColor, positionXone, positionYone, oneside, twoside, thirdside, fourside);
                        shape.draw(g, on_off);
                    }
                    else
                    {
                        MessageBox.Show("Please pass valid parameter for TRIANGLE");
                    }
                }
                else if (readCmd[0].ToUpper() == "WHILE")
                {
                    if (VariableDictionary.ContainsKey(readCmd[1]))
                    {
                        int loopstart = index + 1;
                        int whileStart = loopstart;


                        string[] readCommand = multiLine[index].Split(' ', ',');
                        while (readCommand[0].ToUpper() != "ENDLOOP")
                        {
                            readCommand = multiLine[whileStart].Split(' ', ',');
                            whileStart = whileStart + 1;
                        }

                        int loopEnd = whileStart - 1;
                        MessageBox.Show(loopstart.ToString() + loopEnd);

                        int variableDic = VariableDictionary[readCmd[1]];
                        MessageBox.Show(variableDic.ToString());
                        if (readCmd[2] == ">")
                        {
                            while (variableDic > int.Parse(readCmd[3]))
                            {
                                multiCommand(multiCmd, g, loopstart, loopEnd);
                                variableDic = VariableDictionary[readCmd[1]];
                            }

                        }
                        else if (readCmd[2] == "<")
                        {
                            while (variableDic < int.Parse(readCmd[3]))
                            {
                                multiCommand(multiCmd, g, loopstart, loopEnd);
                                variableDic = VariableDictionary[readCmd[1]];
                            }
                        }
                        else if (readCmd[2] == "==")
                        {
                            while (variableDic == int.Parse(readCmd[3]))
                            {
                                multiCommand(multiCmd, g, loopstart, loopEnd);
                                variableDic = VariableDictionary[readCmd[1]];
                            }
                        }
                        else
                        {
                            /*MessageBox.Show("Invalid comparator passed");*/
                        }
                    }
                }
                else if (readCmd[0].ToUpper() == "IF")//words=readcmd
                {
                    if (readCmd.Contains("THEN"))
                    {
                        String temp = string.Join(" ", readCmd);
                        String temp1 = string.Join(" ", readCmd[0], readCmd[1], readCmd[2], readCmd[3], readCmd[4]);
                        String ifStatement = temp.Replace(temp1, "");

                        if (readCmd[2] == "==")
                        {
                            if (VariableDictionary[readCmd[1]] == Int32.Parse(readCmd[3]))
                            {
                                multiCommand(ifStatement, g, 0, 1);
                            }
                        }
                        if (readCmd[2] == ">")
                        {
                            if (VariableDictionary[readCmd[1]] > Int32.Parse(readCmd[3]))
                            {
                                multiCommand(ifStatement, g, 0, 1);
                            }
                        }
                        if (readCmd[2] == "<")
                        {
                            if (VariableDictionary[readCmd[1]] < Int32.Parse(readCmd[3]))
                            {
                                multiCommand(ifStatement, g, 0, 1);
                            }
                        }
                        if (readCmd[2] == ">=")
                        {
                            if (VariableDictionary[readCmd[1]] >= Int32.Parse(readCmd[3]))
                            {
                                multiCommand(ifStatement, g, 0, 1);
                            }
                        }
                        if (readCmd[2] == "<=")
                        {
                            if (VariableDictionary[readCmd[1]] <= Int32.Parse(readCmd[3]))
                            {
                                multiCommand(ifStatement, g, 0, 1);
                            }
                        }
                        if (readCmd[2] == "!=")
                        {
                            if (VariableDictionary[readCmd[1]] != Int32.Parse(readCmd[3]))
                            {
                                multiCommand(ifStatement, g, 0, 1);
                            }
                        }
                    }
                    else
                    {
                        int startLine = index + 1;
                        int testline = startLine;
                        String[] words1 = multiLine[index].Trim().Split(' ', ',');

                        while (words1[0].ToUpper() != "ENDIF")
                        {
                            words1 = multiLine[testline].Trim().Split(' ', ',');
                            testline++;
                        }

                        int endLine = testline;

                        if (readCmd[2] == "==")
                        {
                            if (VariableDictionary[readCmd[1]] == Int32.Parse(readCmd[3]))
                            {
                                multiCommand(multiCmd, g, startLine, endLine);
                            }
                        }
                        if (readCmd[2] == ">")
                        {
                            if (VariableDictionary[readCmd[1]] > Int32.Parse(readCmd[3]))
                            {
                                multiCommand(multiCmd, g, startLine, endLine);
                            }
                        }
                        if (readCmd[2] == "<")
                        {
                            if (VariableDictionary[readCmd[1]] < Int32.Parse(readCmd[3]))
                            {
                                multiCommand(multiCmd, g, startLine, endLine);
                            }
                        }

                    }
                }
                else
                {
                    /* MessageBox.Show(readCmd[0]);
                    MessageBox.Show(readCmd[1]);
                    MessageBox.Show(readCmd[2]);*/
                    if (readCmd.Length == 3)
                    {
                        variable = readCmd[0];
                        comparator = readCmd[1];
                        /*value = readCmd[2];*/
                        if (comparator == "=")
                        {
                            VariableDictionary.Add(variable, int.Parse(readCmd[2]));

                        }
                    }
                    else if (readCmd[0] == readCmd[2])
                    {
                        if (readCmd[3] == "+")
                        {
                            VariableDictionary[readCmd[0]] = VariableDictionary[readCmd[0]] + int.Parse(readCmd[4]);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please pass valid command to execute");
                    }
                }
            }
        }
        
    }
}
