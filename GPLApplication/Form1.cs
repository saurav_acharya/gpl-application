﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GPLApplication
{
    public partial class Form1 : Form
    {
       
      /*  Pen p = new Pen(Color.Black, 2);*/
        Graphics g;
        
        public Form1()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
            
            
        }
       
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            
        }
        /// <summary>
        /// helps to save data in .txt file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamWriter fWriter = File.CreateText("H:\\GPLApplication.txt");
            fWriter.Write(richTextBox1.Text);
            fWriter.Close();

            MessageBox.Show("File Saved");
        }
        /// <summary>
        /// helps to load data from the saved file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                StreamReader s = File.OpenText("H:\\GPLApplication.txt");
                do
                {
                    string line = s.ReadLine();
                    if (line == null) break;
                    richTextBox1.Text += line + "\n";
                    //Note, we could have used
                    //richTextBox1+=s.ReadToEnd();

                } while (true);
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("ERROR", "Cannot find GPLAplication.txt");
            }
            catch (IOException ie)
            {
                MessageBox.Show("Error", "IO Exception ");
            }
        }
        /// <summary>
        /// to exit the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This application helps to draw shape as you wish by passing the command through command line");
        }

        /// <summary>
        /// creating object of the class CommandParser
        /// Passing the value of text boxes in commandCode method of CommandParser class after passing enter key from keyboard
        /// Refresh picture box aftre entering
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int numberOfLines = richTextBox1.Lines.Length;
                string singleLine = textBox1.Text.ToUpper().Trim();
                pictureBox1.Refresh();

                CommandParser cmd = new CommandParser();
                string multiLine = richTextBox1.Text;
                cmd.CommandCode(singleLine, multiLine, g, 0, numberOfLines);


                /*Refresh();*/
            }
        }

        
    }
}
