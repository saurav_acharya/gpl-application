﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using GPLApplication;
using System.Drawing;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        CommandParser cmd = new CommandParser();
        Graphics g;
        readonly drawTo Test;
        [TestMethod]
        public void TestMethod1()
        {
            /*string singleLine = "rectangle 10,40";
            int onePont = 0;
            int twoPoint = 0;
            string width = 10.ToString();
            string height = 40.ToString();
            string[] expectedArray = {"rectangle",width,height};
            *//*;
            expectedArray[1].ToString();*//*

            CommandParser cp = new CommandParser();
            string[] ActualArray=cp.CommandLoad(singleLine, onePont, twoPoint);
            Assert.AreNotEqual(ActualArray, expectedArray);*/
        }
        [TestMethod]
        public void TestInvalidCommand()
        {
            //Sets variables
            String Command = "move to ";
            String[] CommandSplit = new String[100];
            CommandSplit = Command.Split(' ');
            //Checks if the statements are true as expected
            Assert.AreNotEqual("moveto.", CommandSplit[0]);
        }

        [TestMethod]
        public void InitializeVariableTest()
        {
                      
            String variableName = "width";
            int VariableValue = 100;
            string variable1 = "";
            int value1 = 0;
            string command = "width = 100";
            /* cmd.CommandSplit = "width = 100".Split(' ');
             cmd.command();*/
            cmd.multiCommand(command, g,0, 1);
            //Passes values as parameters to Test.MoveTo
            foreach (KeyValuePair<string, int> variable in cmd.VariableDictionary)
            {
                if (variable.Key.Equals(variableName))
                {
                    variable1 = variable.Key;
                    value1 = variable.Value;
                }
            }
            Assert.AreEqual(variableName, variable1);
            Assert.AreEqual(VariableValue, value1);

        }



    }
}
